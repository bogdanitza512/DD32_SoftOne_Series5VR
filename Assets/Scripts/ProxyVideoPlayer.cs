﻿using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

/// <summary>
/// 
/// </summary>
[RequireComponent(typeof(VideoPlayer), typeof(RawImage))]
public class ProxyVideoPlayer : MonoBehaviour
{

    #region Nested Types

    [System.Serializable]
    public struct VideoSpecs
    {
        public VideoClip clip;
        public Sprite splashScreen;
        public string proxyTriggerName;
    }

    #endregion

    #region Fields and Properties

    [SerializeField]
    int currentVideoIndex;

    [SerializeField]
    List<VideoSpecs> playlist = new List<VideoSpecs>();

    [SerializeField]
    Animator proxyElementsAnimtor;

    [SerializeField]
    string idleProxyTriggerName = "Trigger_Idle";

    public RawImage display;

    public Image splashScreen;

    public VideoPlayer videoPlayer;

    public Button playButton;

    [SerializeField]
    float fadeDuration = 1;

    [SerializeField]
    bool isPlayerInitialized;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    /// void Start() { }

    private void OnEnable()
    {
        playButton.onClick.AddListener(OnPlayButton_Click);

        videoPlayer.started += OnVideoPlayer_Started;

        videoPlayer.loopPointReached += OnVideoPlayer_LoopPointReached;
    }

    private void OnDisable()
    {
        playButton.onClick.RemoveListener(OnPlayButton_Click);

        videoPlayer.started -= OnVideoPlayer_Started;

        videoPlayer.loopPointReached -= OnVideoPlayer_LoopPointReached;
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    /// void Update() { }

    #endregion

    #region Methods

    [Button]
    private void SetupRefs()
    {
        if (videoPlayer == null)
        {
            videoPlayer = GetComponent<VideoPlayer>();
        }

        if (display == null)
        {
            display = GetComponent<RawImage>();
        }
    }

    private void OnVideoPlayer_Started(VideoPlayer source)
    {
        display.DOFade(1, fadeDuration);
        // playButton.image.DOFade(0, fadeDuration);
        splashScreen.DOFade(0, fadeDuration);
    }

    private void OnVideoPlayer_LoopPointReached(VideoPlayer source)
    {
        display.DOFade(0, fadeDuration);
        playButton.image.DOFade(1, fadeDuration);
        splashScreen.DOFade(1, fadeDuration);
        isPlayerInitialized = false;
        proxyElementsAnimtor.SetTrigger(idleProxyTriggerName);
    }

    public void PlayVideo(int index)
    {
        currentVideoIndex = index;
        OpenProxyVideo(playlist[index]);
    }

    public void OpenProxyVideo(VideoSpecs specs)
    {
        // Pause();
         
        videoPlayer.clip = specs.clip;

        splashScreen.sprite = specs.splashScreen;

        // proxyElementsAnimtor.SetTrigger(idleProxyTriggerName);
        // proxyElementsAnimtor.StopPlayback();

        proxyElementsAnimtor.SetTrigger(specs.proxyTriggerName);

        isPlayerInitialized = true;

        Play();
    }


    public void OnPlayButton_Click()
    {
        if(videoPlayer.isPlaying)
        {
            Pause();
        }
        else
        {
            Play();
        }
    }

    public void Play()
    {
        if (isPlayerInitialized)
        {
            videoPlayer.Play();
            proxyElementsAnimtor.speed = 1;
            playButton.image.DOFade(0, fadeDuration);
        }
        else
        {
            PlayVideo(currentVideoIndex);
        }
            
    }

    public void Pause()
    {
        videoPlayer.Pause();
        proxyElementsAnimtor.speed = 0;
        playButton.image.DOFade(1, fadeDuration);
    }



    #endregion
}