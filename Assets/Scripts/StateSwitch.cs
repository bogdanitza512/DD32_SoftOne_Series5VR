﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class StateSwitch : MonoBehaviour
{
    #region Nested Types

    [System.Serializable]
    public struct StateSpecs
    {
        public Button button;
        public string proxyTriggerName;
    }

    [System.Serializable]
    public class OnStateSwitchedEvent : UnityEvent<int> { }

    #endregion

    #region Fields and Properties

    [SerializeField]
    Animator switchAnim;

    [SerializeField]
    int currentState;

    [SerializeField]
    List<StateSpecs> stateList = new List<StateSpecs>();

    public OnStateSwitchedEvent onStateSwitched;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        foreach (StateSpecs state in stateList)
        {
            state.button.onClick.AddListener(() =>
            {
                OnSwitchToState(stateList.IndexOf(state));
            });
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    /// void Update() { }

    /// <summary>
    /// This function is called when the object becomes enabled and active
    /// </summary>
    /// private void OnEnable() { }

    /// <summary>
    /// This function is called when the behaviour becomes disabled or inactive
    /// </summary>
    /// private void OnDisable() { }

    #endregion

    #region Methods

    public void OnSwitchToState(int state)
    {
        if (state != currentState)
        {
            currentState = state;
            switchAnim.SetTrigger(stateList[currentState].proxyTriggerName);
            onStateSwitched.Invoke(state);
        }
    }

    #endregion
}