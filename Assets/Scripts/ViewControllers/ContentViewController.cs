﻿using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.UI;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class ContentViewController : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public List<UIView> views;

    public int currentViewIndex;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    // private void Start() { }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    // private void Update(){ }

    #endregion

    #region Methods

    public void OnNavBarButton_Click(int viewIndex)
    {
        if (currentViewIndex != viewIndex && views.Count > viewIndex)
        {
            views[viewIndex].Show();

            views[currentViewIndex].Hide();

            currentViewIndex = viewIndex;
        }
    }

    public void ShowFirstScreen()
    {
        views[0].Show();
        currentViewIndex = 0;
    }
	

    #endregion

}
