﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// 
/// </summary>
public class AnimatorEventDispatch : MonoBehaviour {

    #region Nested Types
        
	public UnityEvent onEventSignal;

    #endregion

    #region Fields and Properties

	

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    /// private void Start() {  }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    /// private void Update() {  }

    #endregion

    #region Methods

    public void InvokeUnityEvent()
    {
        onEventSignal.Invoke();
    }
	

    #endregion

}
