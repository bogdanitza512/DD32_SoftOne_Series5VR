﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;
using BN512.Extensions;

/// <summary>
/// 
/// </summary>
[ExecuteInEditMode]
public class PopUpElement : SerializedMonoBehaviour
{

    #region Fields and Properties

    [SerializeField]
    string activeImageName = "[Image_Active]";

    [SerializeField]
	Image activeImage;

    [SerializeField]
    string inactiveImageName = "[Image_Inactive]";

    [SerializeField]
    Image inactiveImage;

    [SerializeField]
    string labelName = "[Text_Label]";

    [SerializeField]
    TMP_Text label;

    [SerializeField]
    Color activeColor = Color.white;

    [SerializeField]
    Color inactiveColor = Color.white;


    [SerializeField, Range(0,1)]
	float _alpha;

	float _lastAlpha;

    void RefreshAlpha()
	{
		activeImage.color =
                           activeImage.color.WithAlpha(Mathf.Lerp(0, 1, _alpha));
        inactiveImage.color =
                         inactiveImage.color.WithAlpha(Mathf.Lerp(0, 1, 1 - _alpha));
		label.color = Color.Lerp(inactiveColor, activeColor, _alpha);
	}

    //[ShowInInspector, PropertyRange(0,1)]
    public float activeAlpha
	{
		get { return _alpha; }
		set 
		{ 
			_alpha = value;
			RefreshAlpha();
		}
	}

    [ShowInInspector] 
    public string LabelText
    {
        get => label.text;
        set => label.text = value;
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    // void Start() { }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if(Mathf.Abs(_alpha - _lastAlpha) > 0.01)
        {
            RefreshAlpha();
            _lastAlpha = _alpha;
        }
    }

    #endregion

    #region Methods

	[Button]
    void SetupRefs()
    {
        foreach(Transform child in transform)
        {
            if(activeImage == null &&
               child.name == activeImageName)
            {
                activeImage = child.GetComponent<Image>();
            }

            if (inactiveImage == null &&
                child.name == inactiveImageName)
            {
                inactiveImage = child.GetComponent<Image>();
            }

            if (label == null &&
                child.name == labelName)
            {
                label = child.GetComponent<TMP_Text>();
            }
        }
    }

    #endregion
}
